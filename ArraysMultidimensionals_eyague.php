<!DOCTYPE html>
<html lang="ca">

    <head>
        <meta charset="UTF-8">
        <title>Arrays</title>
                                                                                               
    </head>

    <body>

        <?php

        function ArrayMultidimensional($n) {
            $arrayMulti = array();
            for ($contador1 = 0; $contador1 < $n; $contador1++) {
                for ($contador2 = 0; $contador2 < $n; $contador2++) {
                    if ($contador1 == $contador2) {
                        $arrayMulti[$contador1][$contador2] = "*";
                    }
                    if ($contador1 < $contador2) {
                        $arrayMulti[$contador1][$contador2] = $contador1 + $contador2;
                    }
                    if ($contador1 > $contador2) {
                        $arrayMulti[$contador1][$contador2] = rand(10, 20);
                    }
                }
            }
            MostrarArrayATaula($arrayMulti);
            ArrayGirada($arrayMulti);
        }

        function MostrarArrayATaula($arrayMulti) {
            echo("<table border=\"1px solid black\">");
            for ($contador1 = 0; $contador1 < count($arrayMulti); $contador1++) {
                echo("<tr>");
                for ($contador2 = 0; $contador2 < count($arrayMulti); $contador2++) {
                    echo("<td>" . $arrayMulti[$contador1][$contador2] . "</td>");
                }
                echo("</tr>");
            }
            echo("</table>");
        }

        function ArrayGirada($arrayMulti) {
            $arrayMulti2 = array();
            foreach ($arrayMulti as $fila1 => $columna1) {
                foreach ($columna1 as $fila2 => $columna2) {
                    $arrayMulti2[$fila2][$fila1] = $columna2;
                }
            }
            MostrarArrayATaula($arrayMulti2);
        }

        ArrayMultidimensional(4);
        ?>
    </body>

</html>