<!DOCTYPE html>
<html lang="ca">

    <head>
        <meta charset="UTF-8">
        <title>Arrays</title>

    </head>

    <body>

        <?php
        $factArray = array(1, 5, 100);
        $novaArray = "";

        function hola($factArray) {
            for ($index = 0; $index < count($factArray); $index++) {
                if (!is_array($factArray) && !is_int($factArray[$index])) {
                    return false;
                } else {
                    $novaArray[$index] = FactorialArray($factArray[$index]);
                    echo "<p>" . $novaArray[$index] . "</p>";
                }
            }

            return $novaArray;
        }

        function FactorialArray($numFactorial) {
            if ($numFactorial == 0) {
                return 1;
            }
            return $numFactorial * FactorialArray($numFactorial - 1);
        }

        hola($factArray);
        ?>
    </body>

</html>